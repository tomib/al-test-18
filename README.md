# AL Test Tasks

### Installation

After cloning or downloading the repository, navigate to the project root and run:

```
npm install
```

### Usage

Once the dependencies are installed, run the tests with:

```
npm run test
```

```
npm run task0
```

```
npm run task1
```

```
npm run task2
```