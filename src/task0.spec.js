const { assert } = require("chai");
const maskCreditCard = require("./task0");

describe("maskCreditCard", () => {
  it("masks the digits of standard credit cards", () => {
    assert.equal(maskCreditCard("5512103073210694"), "5###########0694");
  });

  it("does not mask the digits of short credit cards", () => {
    assert.equal(maskCreditCard("54321"), "54321");
  });

  it("does not mask non-digits", () => {
    assert.equal(maskCreditCard("4556-3646-0793-5616"), "4###-####-####-5616");
  });

  it("returns empty string if arg is falsey ", () => {
    assert.equal(maskCreditCard(""), "");
  });

  it("does not mask Skippy", () => {
    assert.equal(maskCreditCard("Skippy"), "Skippy");
  });

  it("does not mask spaces", () => {
    assert.equal(maskCreditCard("Nananananananananananananananana Batman!"), "Nananananananananananananananana Batman!");
  });
});
