const { assert } = require("chai");
const numberToOrdinal = require("./task1");

describe("numberToOrdinal", () => {
  it("should return 0 when called with 0", () => {
    assert.equal(numberToOrdinal(0), "0");
  });

  it("should return 1st when called with 1", () => {
    assert.equal(numberToOrdinal(1), "1st");
  });

  it("should return 2nd when called with 2", () => {
    assert.equal(numberToOrdinal(2), "2nd");
  });

  it("should return 3rd when called with 3", () => {
    assert.equal(numberToOrdinal(3), "3rd");
  });

  it("should return 4th when called with 4", () => {
    assert.equal(numberToOrdinal(4), "4th");
  });

  it("should return 124th when called with 124", () => {
    assert.equal(numberToOrdinal(124), "124th");
  });

  it("should return 991st when called with 991", () => {
    assert.equal(numberToOrdinal(991), "991st");
  });

  it("should return 112th when called with 112", () => {
    assert.equal(numberToOrdinal(112), "112th");
  });

  it("should return 900th when called with 900", () => {
    assert.equal(numberToOrdinal(900), "900th");
  });
});
