/**
 * Masks all digits in a given string except the first one and the last four
 *
 * Examples:
 *   1) 5512103073210694 -> 5###########0694
 *   2) 4556-3646-0793-5616 -> 4###-####-####-5616
 *   3) 1-2-3-4-5-6-7-8 -> 1-#-#-#-5-6-7-8
 */
module.exports = function maskCreditCard (creditCard) {
  if (creditCard && creditCard.trim().length > 0) {
    var digitPositions = [], maskedCreditCard = creditCard.split('');

    /* Generate an array of all positions in the string where numbers are located. */
    for (var i = 0; i < creditCard.length; i++) {
      if (!isNaN(creditCard[i])) {
        digitPositions.push(i);
      }
    }

    /* Go through the detected positions and mask all numbers located there except the first and last four. */
    for (var i = 0; i < digitPositions.length; i++) {
      if (i !== 0 && i < (digitPositions.length - 4)) {
        maskedCreditCard[digitPositions[i]] = '#';
      }
    }

    /* Convert back to string and return. */
    return maskedCreditCard.join('');
 
  } else {
    return '';
  }
};
