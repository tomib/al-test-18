const { assert } = require("chai");
const calculate = require("./task2");

describe("calculate", () => {
  it("should parse numbers", () => {
    assert.equal(calculate("1 2 3"), 3);
  });

  it("should parse floats", () => {
    assert.equal(calculate("1 2 3.5"), 3.5);
  });

  it("should support addition", () => {
    assert.equal(calculate("1 3 +"), 4);
  });

  it("should support multiplication", () => {
    assert.equal(calculate("1 3 *"), 3);
  });

  it("should support subtraction", () => {
    assert.equal(calculate("1 3 -"), -2);
  });

  it("should support division", () => {
    assert.equal(calculate("4 2 /"), 2);
  });

  it("returns 6 when passed '7 4 5 + * 3 - 10 /'", () => {
    assert.equal(calculate("7 4 5 + * 3 - 10 /"), 6);
  });

  it("returns 3 when passed '3 2 1 - *'", () => {
    assert.equal(calculate("3 2 1 - *"), 3);
  });
});
