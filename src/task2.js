/**
 * Calculates expression that consists of numbers (integer or float) and operators divided by space, for example:
 *   1) "3 5 - 7 *"
 *   2) "7 4 5 + * 3 - 10 /"
 * Each operator gets applied to the 2 most recent numbers, for example:
 *   1) "3 1 -" -> 2
 *   2) "3 1 - 7 *" -> "2 7 *" -> 14
 * Supported operators are: + - * /
 * If there are numbers without an operator following them, the last such number is returned, for example:
 *   "3 1 - 5.2" -> "2 5.2" -> 5.2
 */

/* Parsing a number entered as a string as either Float or Int. */
function parseNumber(number) {
  var floatRegex = new RegExp('^[-+]?[0-9]+\.[0-9]+$');

  if (floatRegex.test(number)) {
    return parseFloat(number);
  } else {
    return parseInt(number);
  }
}

/* Recursion to traverse and reduce the array until no more operations are found. */
function processExpression(expressionArray) {
  /* If the array has not been reduced yet to one element, proceed with recursion. */
  if (expressionArray.length > 1) {
    /* Traverse the array until an operation is found. */
    for (var i = 0; i < expressionArray.length; i++) {
      /* Once an operation has been found, replace the element containing the operation symbol with the result and remove the previous two elements, then re-run the function with the new smaller array. */
      if (expressionArray[i] === '+') {
        expressionArray[i] = parseNumber(expressionArray[i - 2]) + parseNumber(expressionArray[i - 1]);
        expressionArray.splice(i - 2, 2);
        return processExpression(expressionArray);
      } else if (expressionArray[i] === '-') {
        expressionArray[i] = parseNumber(expressionArray[i - 2]) - parseNumber(expressionArray[i - 1]);
        expressionArray.splice(i - 2, 2);
        return processExpression(expressionArray);
      } else if (expressionArray[i] === '*') {
        expressionArray[i] = parseNumber(expressionArray[i - 2]) * parseNumber(expressionArray[i - 1]);
        expressionArray.splice(i - 2, 2);
        return processExpression(expressionArray);
      } else if (expressionArray[i] === '/') {
        expressionArray[i] = parseNumber(expressionArray[i - 2]) / parseNumber(expressionArray[i - 1]);
        expressionArray.splice(i - 2, 2);
        return processExpression(expressionArray);
      /* If no operation has been found, return an array containing only the last digit to trigger exit condition. */
      } else if (i === expressionArray.length - 1) {
        return processExpression([parseNumber(expressionArray[i])]);
      }  
    }
  /* If the array only contains one element, it means that the solution has been found and the result can be returned. */
  } else {
    return expressionArray;
  }
  
}

module.exports = function calculate (expression) {
  if (expression && expression.trim().length > 0) {
    return processExpression(expression.split(' '));
  } else {
    return '';
  }
};
