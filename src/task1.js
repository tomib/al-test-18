/**
 * Appends suffix to a number making it ordinal
 *
 * Examples:
 *   1) 1 -> "1st"
 *   2) 12 -> "12th"
 *   3) 9991 -> "9991st"
 */
module.exports = function numberToOrdinal (n) {
  if (!isNaN(n)) {
    var suffix = '', numberLength = n.toString().length;

    /* If the number is longer than one digit, process it. */
    if (numberLength > 1) {
      /* Convert to array and calculate length. */
      var numberArray = n.toString().split('');

      /* Check the second to last, then last digit. If the end of the number is in the 11-19 range, always return 'th', otherwise return regular suffixes. */
      if (parseInt(numberArray[numberLength - 2]) === 1) {
        suffix = 'th';
      } else {
        switch (parseInt(numberArray[numberLength - 1])) {
          case 0:
            suffix = 'th';
            break;
          case 1:
            suffix = 'st';
            break;
          case 2:
            suffix = 'nd';
            break;
          case 3:
            suffix = 'rd';
            break;
          default:
            suffix = 'th';
            break;
        }
      }
    /* Else if it's single digit, return 0 for 0 and standard suffixes for the rest. */
    } else {
      switch (n) {
        case 0:
          suffix = '';
          break;
        case 1:
          suffix = 'st';
          break;
        case 2:
          suffix = 'nd';
          break;
        case 3:
          suffix = 'rd';
          break;
        default:
          suffix = 'th';
          break;
      }
    }

    return n += suffix;
    
  } else {
    return '';
  }
};
